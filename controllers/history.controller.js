const express = require('express');

const historyService = require('../services/history.service');

exports.getHistory = function (req, res, next) {
    historyService.getHistory(req.params)
        .then( data => res.json(data))
        .catch(err => next(err));
}

exports.getHistoryDetails = function (req, res, next) {
    historyService.getHistoryDetails(req.user.sub)
        .then( data => res.json(data))
        .catch(err => next(err));
}