const express = require("express");
const request = require("request");
const e = require("express");
const { json } = require("body-parser");
const weatherService = require("../services/weather.service");

exports.getWeather = function (req, res, next) {
  let { lat } = req.body;
  let { lng } = req.body;
  let { city } = req.body;
  let user = req.user.sub;

  request(
    `http://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lng}&units=metric&appid=2b7a8efd8eef6e6ca7dbd4539f13bb02`,
    function (error, response, body) {
      let info = JSON.parse(body).list.map((el) => {
        return (obj = {
          weather: el.weather.map((el) => el.main).join(),
          temp: Math.ceil(el.main.temp),
          date: el.dt_txt,
        });
      });

      let weatherParam = { info, city, user }
      weatherService.getWeather(weatherParam)
      .then( user => res.json(user))
     .catch(err => next(err));
    }
  );
  //
};
