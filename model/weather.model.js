const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const WeatherSchema = new mongoose.Schema({
     user: [{ type: Schema.Types.ObjectId, ref: "User" }],
     date: { type: Date, default: Date.now, index: true },
     info: Array,
     city: String,
});

const Weather = mongoose.model("Weather", WeatherSchema);

module.exports = Weather;
