const db = require('_helpers/db');
const { ObjectID } = require('mongodb');
const Weather = db.Weather


exports.getHistory = async function (params) {

    return await Weather.find({user:ObjectID(params.id)}, {info: 1, city: 1, date:1 }).limit(20);  
}


exports.getHistoryDetails = async function (params) {
    return await Weather.findOne({user:ObjectID(params)}, {info: 1, city: 1, date:1 }).sort({"date": -1}); 
}