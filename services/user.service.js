﻿const config = require('config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const db = require('_helpers/db');
const User = db.User;


exports.authenticate = async function ({ email, password }) {
    const user = await User.findOne({ email });
    if (user && bcrypt.compareSync(password, user.password)) {
        const token = jwt.sign({ sub: user.id }, config.secret, { expiresIn: '7d' });
        return {
            ...user.toJSON(),
            token
        };
    }
}



exports.create  = async function (userParam) {
  
    // validate
    if (await User.findOne({ email: userParam.email } )) {
        throw `UserEmail is ${userParam.email} already taken`;
    }
    if (await User.findOne({ username: userParam.username } )) {
        throw `UserName is ${userParam.username} already taken`;
    }
  
    const user = new User(userParam);

    // hash password
    if (userParam.password) {
        user.password = bcrypt.hashSync(userParam.password, 10);
    }

    // save user
    await user.save();
}

exports.update = async function (id, userParam) {
    const user = await User.findById(id);
    const token = jwt.sign({ sub: id }, config.secret, { expiresIn: '7d' });

    // validate
    if (!user) throw 'User not found';

    // hash password if it was entered
    if (userParam.password) {
        userParam.password = bcrypt.hashSync(userParam.password, 10);
    }

    
    // copy userParam properties to user
    Object.assign(user, userParam);
     await user.save()
     return {
        ...user.toJSON(),
        token
     }
}


exports.getById = async function (id) {

    return await User.findById(id);

}