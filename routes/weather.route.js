var express = require('express');
const router = express.Router();

const  WeatherController = require('../controllers/weather.controller')

router.post('/weather', WeatherController.getWeather);

module.exports = router;