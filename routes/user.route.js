var express = require('express');
const router = express.Router();

const  UserController = require('../controllers/users.controller')

router.post('/authenticate', UserController.authenticate);
router.post('/register', UserController.register);
router.put('/:id', UserController.update);
router.get('/:id', UserController.getById);


module.exports = router;