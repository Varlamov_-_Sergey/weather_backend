var express = require('express');
const router = express.Router();

const  HistoryController = require('../controllers/history.controller')

router.get('/history/:id/', HistoryController.getHistory);
router.post('/history-details', HistoryController.getHistoryDetails);


module.exports = router;