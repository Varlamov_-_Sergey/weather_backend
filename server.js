﻿require('rootpath')();
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const jwt = require('./middleware/checkAuth');
const errorHandler = require('_helpers/error-handler');
const user = require('./routes/user.route');
const weather = require('./routes/weather.route');
const history = require('./routes/history.route');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

//middlewate JWT
app.use(jwt());

// api routes
app.use('/', user);
app.use('/', weather);
app.use('/', history)
// global error handler
app.use(errorHandler);

// start server
const port = process.env.NODE_ENV === 'production' ? (process.env.PORT || 80) : 4000;
const server = app.listen(port, function () {
    console.log('Server listening on port ' + port);
});
